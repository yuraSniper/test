#pragma once

#include <Util\std.h>
#include <Util\Math\vec3.h>
#include "Vertex.h"

class Mesh
{
	vector<Vertex> mesh;
	unsigned int vboId;
	vec3 offset;
	bool isUpdated, isUpdating;
public:
	Mesh();
	~Mesh();
	void addVertex(Vertex vert);
	void addVertexArray(Vertex * arr, int count);
	void setOffset(float x, float y, float z);
	void render(bool clear = false, bool useTex = false);
	void clear();
	bool empty();
	void setIsUpdating(bool b);
	bool getIsUpdating();
};