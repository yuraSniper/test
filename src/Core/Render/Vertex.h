#pragma once

struct Vertex
{
	float x, y, z;
	float u, v;
	float r, g, b, a;
	Vertex(float x, float y, float z, float u = 0, float v = 0)
	{
		this->x = x;
		this->y = y;
		this->z = z;
		this->u = u;
		this->v = v;
		r = g = b = a = 1;
	}
	Vertex(float x, float y, float z, float r, float g, float b, float a = 1, float u = 0, float v = 0)
	{
		this->x = x;
		this->y = y;
		this->z = z;
		this->u = u;
		this->v = v;
		this->r = r;
		this->g = g;
		this->b = b;
		this->a = a;
	}
};