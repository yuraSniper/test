#include "Mesh.h"

Mesh::Mesh()
{
	offset.x = offset.y = offset.z = 0;
	vboId = 0;
	isUpdated = true;
	isUpdating = false;
}

Mesh::~Mesh()
{
	if (vboId != 0)
		glDeleteBuffers(1, &vboId);
}

void Mesh::addVertex(Vertex vert)
{
	vert.x += offset.x;
	vert.y += offset.y;
	vert.z += offset.z;
	mesh.push_back(vert);
	isUpdated = true;
}

void Mesh::addVertexArray(Vertex * arr, int count)
{
	for (int i = 0; i < count; i++)
	{
		Vertex vert = arr[i];
		vert.x += offset.x;
		vert.y += offset.y;
		vert.z += offset.z;
		mesh.push_back(vert);
	}
	isUpdated = true;
}

void Mesh::setOffset(float x, float y, float z)
{
	offset.x = x;
	offset.y = y;
	offset.z = z;
}

void Mesh::render(bool clear, bool useTex)
{
	if (mesh.empty())
		return;

	if (vboId == 0)
		glGenBuffers(1, &vboId);

	glBindBuffer(GL_ARRAY_BUFFER, vboId);

	if (isUpdated)
	{
		isUpdated = false;
		glBufferData(GL_ARRAY_BUFFER, mesh.size() * sizeof(Vertex), &(mesh[0]), GL_DYNAMIC_DRAW);
	}

	glEnableClientState(GL_COLOR_ARRAY);
	glVertexPointer(3, GL_FLOAT, sizeof(Vertex), 0);
	glColorPointer(4, GL_FLOAT, sizeof(Vertex), (void *)(5 * sizeof(float)));

	if (useTex)
	{
		glEnableClientState(GL_TEXTURE_COORD_ARRAY);
		glTexCoordPointer(2, GL_FLOAT, sizeof(Vertex), (void *)(3 * sizeof(float)));
	}

	glDrawArrays(GL_QUADS, 0, mesh.size());

	glDisableClientState(GL_COLOR_ARRAY);

	if (useTex)
		glDisableClientState(GL_TEXTURE_COORD_ARRAY);

	if (clear)
	{
		mesh.clear();
		isUpdated = true;
	}
}

void Mesh::clear()
{
	mesh.clear();
	isUpdated = true;
}

bool Mesh::empty()
{
	return mesh.empty();
}

void Mesh::setIsUpdating(bool b)
{
	isUpdating = b;
}

bool Mesh::getIsUpdating()
{
	return isUpdating;
}