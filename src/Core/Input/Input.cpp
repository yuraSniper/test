#include "Input.h"

Input::Input(Window * w)
{
	wnd = w;
	dw = 0;
	for (int i = 0; i < 256; i++)
	{
		keys[i] = false;
		keyPressed[i] = false;
	}
	for (int i = 0; i < 2; i++)
	{
		buttons[i] = false;
		pos[i] = 0;
	}
	w->onButtonDown.add(onButtonDown, this);
	w->onButtonUp.add(onButtonUp, this);
	w->onKey.add(onKey, this);
	w->onMouseMove.add(onMouseMove, this);
	w->onMouseWheel.add(onMouseWheel, this);
}

bool Input::GetKey(unsigned char key)
{
	return keys[key];
}

bool Input::GetKeyPressed(unsigned char key)
{
	if (!keys[key] && keyPressed[key])
	{
		keyPressed[key] = false;
		return true;
	}
	return false;
}

bool Input::GetButton(unsigned char button)
{
	button %= 2;
	return buttons[button];
}

unsigned int Input::GetMousePos(bool x)
{
	return x? pos[0] : pos[1];
}

int Input::getMouseWheelDelta()
{
	int a = dw;
	dw = 0;
	return a;
}

void Input::onKey(void * param, void *)
{
	static Input * This = (Input *)param;
	unsigned char k[256];
	GetKeyboardState(k);
	for (int i = 0; i < 256; i++)
	{
		k[i] >>= 7;
		This->keys[i] = k[i];

		if (k[i])
			This->keyPressed[i] = true;
	}
}

void Input::onButtonDown(void * param, void * p)
{
	static Input * This = (Input *)param;
	This->buttons[*(unsigned char *)p] = true;
}

void Input::onButtonUp(void * param, void * p)
{
	static Input * This = (Input *)param;
	This->buttons[*(unsigned char *)p] = false;
}

void Input::onMouseMove(void * param, void * p)
{
	static Input * This = (Input *)param;
	unsigned int * pos = (unsigned int *)p;
	This->pos[0] = pos[0];
	This->pos[1] = pos[1];
}

void Input::onMouseWheel(void * param, void * p)
{
	static Input * This = (Input *)param;
	This->dw = *(int *)p / 120;
}