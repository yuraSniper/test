#pragma once

#include <Util\std.h>
#include <Core\Window\Window.h>

class Input
{
	Window * wnd;
	bool keys[256];
	bool keyPressed[256];
	bool buttons[2];
	unsigned int pos[2];
	int dw;

	static void onKey(void *, void *);
	static void onButtonDown(void *, void *);
	static void onButtonUp(void *, void *);
	static void onMouseMove(void *, void *);
	static void onMouseWheel(void *, void *);
public:
	Input(Window *);
	bool GetKey(unsigned char);
	bool GetKeyPressed(unsigned char);
	bool GetButton(unsigned char);
	unsigned int GetMousePos(bool x);
	int getMouseWheelDelta();
};