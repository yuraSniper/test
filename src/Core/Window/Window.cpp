#include "Window.h"

Window::Window(HINSTANCE _hInst, LPWSTR title, unsigned int width, unsigned int height)
{
	hInst = _hInst;
	RECT rc;
	rc.left = 0;
	rc.right = width;
	rc.top = 0;
	rc.bottom = height;
	AdjustWindowRect(&rc, WS_OVERLAPPEDWINDOW, 0);
	setupWndClass(L"SearchPathWndClass");
	hWnd = CreateWindow(L"SearchPathWndClass", title, WS_OVERLAPPEDWINDOW,
		100, 50, rc.right - rc.left, rc.bottom - rc.top, nullptr, nullptr, hInst, 0);
	SetProp(hWnd, L"This", this);
	ShowWindow(hWnd, SW_SHOWNORMAL);

	InitGL();

	GetClientRect(hWnd, &rc);
	ResizeScene(rc.right - rc.left, rc.bottom - rc.top);
	onDraw = nullptr;
}

void Window::showCursor(bool x)
{
	ShowCursor(x);
}

void Window::setTitle(const string & title)
{
	SetWindowTextA(hWnd, title.c_str());
}

string & Window::getTitle()
{
	char str[257];
	GetWindowTextA(hWnd, str, 64);
	return string(str);
}

HDC Window::GetDC()
{
	return hDC;
}

HGLRC Window::GetRC()
{
	return hRC;
}

HWND Window::GetHWND()
{
	return hWnd;
}

void Window::setupWndClass(LPWSTR cls)
{
	WNDCLASS wc = {0};
	wc.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);
	wc.hCursor = LoadCursor(nullptr, IDC_ARROW);
	wc.hIcon = LoadIcon(nullptr, IDI_APPLICATION);
	wc.hInstance = hInst;
	wc.lpfnWndProc = WndProc;
	wc.lpszClassName = cls;
	wc.style = CS_VREDRAW | CS_HREDRAW | CS_OWNDC;

	RegisterClass(&wc);
}

void Window::InitGL()
{
	PIXELFORMATDESCRIPTOR pfd = {0};
	int pf;

	hDC = ::GetDC(hWnd);

	pfd.nVersion = 1;
	pfd.nSize = sizeof(pfd);
	pfd.cColorBits = 32;
	pfd.dwFlags = PFD_DOUBLEBUFFER | PFD_SUPPORT_OPENGL | PFD_DRAW_TO_WINDOW;
	pfd.iPixelType = PFD_TYPE_RGBA;

	pf = ChoosePixelFormat(hDC, &pfd);
	SetPixelFormat(hDC, pf, &pfd);

	hRC = wglCreateContext(hDC);
	wglMakeCurrent(hDC, hRC);

	glShadeModel(GL_SMOOTH);
	glClearColor(0, 0, 0, 0);

	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glClearDepth(1);

	glEnable(GL_CULL_FACE);

	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
}

void Window::ResizeScene(unsigned int width, unsigned int height)
{
	if (!height)
		height = 1;
	glViewport(0, 0, width, height);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	//glOrtho(0, 1, -1, 0, 0, 1);
	gluPerspective(45, (double)width/(double)height, 0.1, 4000);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
}

LRESULT Window::WndProc(HWND hWnd, unsigned int msg, WPARAM wParam, LPARAM lParam)
{
	Window * This = (Window *)GetProp(hWnd, L"This");
	static unsigned int pos[2];
	static unsigned char key;
	static int deltaWheel;
	switch (msg)
	{
		case WM_MOUSEMOVE:
			pos[0] = GET_X_LPARAM(lParam);
			pos[1] = GET_Y_LPARAM(lParam);
			This->onMouseMove(pos);
			return 0;
		case WM_KEYDOWN:
		case WM_KEYUP:
			This->onKey(nullptr);
			return 0;
		case WM_MOUSEWHEEL:
			deltaWheel = GET_WHEEL_DELTA_WPARAM(wParam);
			This->onMouseWheel(&deltaWheel);
			return 0;
		case WM_LBUTTONDOWN:
		case WM_RBUTTONDOWN:
			key = (msg - WM_LBUTTONDOWN) % 2;
			This->onButtonDown(&key);
			return 0;
		case WM_LBUTTONUP:
		case WM_RBUTTONUP:
			key = (msg - WM_LBUTTONUP) % 2;
			This->onButtonUp(&key);
			return 0;
		/*case WM_PAINT:
			if (This->onDraw)
				(*This->onDraw)(nullptr);
			return 0;*/
		case WM_SIZING:
		case WM_SIZE:
			RECT rc;
			GetClientRect(hWnd, &rc);
			This->ResizeScene(rc.right - rc.left, rc.bottom - rc.top);
			return 0;
		case WM_DESTROY:
			RemoveProp(hWnd, L"This");
			PostQuitMessage(0);
			return 0;
		case WM_CREATE:
			return 0;
	}
	return DefWindowProc(hWnd, msg, wParam, lParam);
}