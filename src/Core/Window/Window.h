#pragma once

#include <Util\std.h>
#include <Util\Delegate.h>

class Window
{
	HINSTANCE hInst;
	HWND hWnd;
	HDC hDC;
	HGLRC hRC;

	void setupWndClass(LPWSTR);
	void InitGL();
	void ResizeScene(unsigned int, unsigned int);

	static LRESULT __stdcall WndProc(HWND, unsigned int, WPARAM, LPARAM);
public:
	Delegate *onDraw, onKey, onButtonDown, onButtonUp,
		onMouseMove, onMouseWheel;
	Window(HINSTANCE, LPWSTR, unsigned int, unsigned int);
	HDC GetDC();
	HGLRC GetRC();
	HWND GetHWND();
	void setTitle(const string & title);
	string & getTitle();
	void showCursor(bool x);
};