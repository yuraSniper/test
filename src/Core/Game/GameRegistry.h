#pragma once

#include <Core\Game\Block.h>
class Block;

class GameRegistry
{
	static Block * blocks[1024];
public:
	static void registerBlock(unsigned int id, Block * block);
	static Block * getBlock(unsigned int id);
};