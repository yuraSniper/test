#include "Block.h"

Block::Block(unsigned int _id) : id(_id)
{
}

void Block::render(World * world, int x, int y, int z)
{
	static Vertex top[] = {
		Vertex(0, 1, 0),
		Vertex(0, 1, 1),
		Vertex(1, 1, 1),
		Vertex(1, 1, 0)
	};
	static Vertex bottom[] = {
		Vertex(0, 0, 1, 0.6, 0.6, 0.6, 0.6),
		Vertex(0, 0, 0, 0.6, 0.6, 0.6, 0.6),
		Vertex(1, 0, 0, 0.6, 0.6, 0.6, 0.6),
		Vertex(1, 0, 1, 0.6, 0.6, 0.6, 0.6)
	};
	static Vertex north[] = {
		Vertex(1, 1, 0, 0.8, 0.8, 0.8),
		Vertex(1, 0, 0, 0.8, 0.8, 0.8),
		Vertex(0, 0, 0, 0.8, 0.8, 0.8),
		Vertex(0, 1, 0, 0.8, 0.8, 0.8)
	};
	static Vertex west[] = {
		Vertex(0, 1, 0, 0.8, 0.8, 0.8),
		Vertex(0, 0, 0, 0.8, 0.8, 0.8),
		Vertex(0, 0, 1, 0.8, 0.8, 0.8),
		Vertex(0, 1, 1, 0.8, 0.8, 0.8)
	};
	static Vertex south[] = {
		Vertex(0, 1, 1, 0.8, 0.8, 0.8),
		Vertex(0, 0, 1, 0.8, 0.8, 0.8),
		Vertex(1, 0, 1, 0.8, 0.8, 0.8),
		Vertex(1, 1, 1, 0.8, 0.8, 0.8)
	};
	static Vertex east[] = {
		Vertex(1, 1, 1, 0.8, 0.8, 0.8),
		Vertex(1, 0, 1, 0.8, 0.8, 0.8),
		Vertex(1, 0, 0, 0.8, 0.8, 0.8),
		Vertex(1, 1, 0, 0.8, 0.8, 0.8)
	};
	Chunk * chunk = world->getChunkFromBlock(x, y, z);
	if (shouldRenderSide(world, x, y, z, 0))
		chunk->getMesh()->addVertexArray(top, 4);
	if (shouldRenderSide(world, x, y, z, 1))
		chunk->getMesh()->addVertexArray(bottom, 4);
	if (shouldRenderSide(world, x, y, z, 2))
		chunk->getMesh()->addVertexArray(north, 4);
	if (shouldRenderSide(world, x, y, z, 3))
		chunk->getMesh()->addVertexArray(west, 4);
	if (shouldRenderSide(world, x, y, z, 4))
		chunk->getMesh()->addVertexArray(south, 4);
	if (shouldRenderSide(world, x, y, z, 5))
		chunk->getMesh()->addVertexArray(east, 4);
}

bool Block::shouldRenderSide(World * world, int x, int y, int z, int side)
{
	switch (side)
	{
		//Top
		case 0:
			if (world->getBlockId(x, y + 1, z) != 0)
				return false;
			break;
		//Bottom
		case 1:
			if (world->getBlockId(x, y - 1, z) != 0)
				return false;
			break;
		//North
		case 2:
			if (world->getBlockId(x, y, z - 1) != 0)
				return false;
			break;
		//West
		case 3:
			if (world->getBlockId(x - 1, y, z) != 0)
				return false;
			break;
		//South
		case 4:
			if (world->getBlockId(x, y, z + 1) != 0)
				return false;
			break;
		//East
		case 5:
			if (world->getBlockId(x + 1, y, z) != 0)
				return false;
			break;
	}
	return true;
}