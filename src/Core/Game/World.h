#pragma once

#include <Util\std.h>
#include <Core\Game\Chunk.h>

class Chunk;

class World
{
	map<unsigned __int64, Chunk *> chunks;
	unsigned __int64 chunkHash(int x, int z);
	CRITICAL_SECTION worldUpdate;
public:
	World();
	static const int renderRadius = 4;
	Chunk * getChunk(int x, int z, bool load = false);
	Chunk * getChunkFromBlock(int x, int y, int z, bool load = false);
	bool loadChunk(int x, int z);
	bool unloadChunk(int x, int z);
	int getBlockId(int x, int y, int z);
	void setBlockId(int id, int x, int y, int z);
	void updateChunk(int x, int z);
	void renderFromPoint(int x, int y, int z);
};