#pragma once

#include <Util\std.h>
#include <Core\Game\GameRegistry.h>
#include <Core\Game\World.h>
#include <Core\Render\Mesh.h>

class World;

class Block
{
public:
	const unsigned int id;
	Block(unsigned int _id);
	void render(World * world, int x, int y, int z);
	bool shouldRenderSide(World * world, int x, int y, int z, int side);
};