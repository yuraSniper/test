#pragma once

#include "Block.h"
#include <Util\Math\Perlin\PerlinNoise.h>
#include <Core\Render\Mesh.h>
#include <Core\TaskQueue.h>
#include <Core\Game\World.h>

class World;

class Chunk
{
public:
	static const int sizePow = 4, heightPow = 7, size = 1 << sizePow, height = 1 << heightPow;
private:
	int *blocks;
	unsigned int chunkX, chunkZ;
	Mesh * mesh;
public:
	Chunk * north, * south, * east, * west;
	World * world;
	Chunk(int x, int z);
	~Chunk();
	int getBlockId(int x, int y, int z);
	void setBlockId(int id, int x, int y, int z);
	Mesh * getMesh();
	void onUpdate();
};