#include "World.h"

World::World()
{
	InitializeCriticalSection(&worldUpdate);
}

unsigned __int64 World::chunkHash(int x, int z)
{
	return (((unsigned __int64)(x & 0x7fffffff)) << 32) | 
		((unsigned __int64)(z & 0x7fffffff));
}

Chunk * World::getChunk(int x, int z, bool load)
{
	if (x < 0 || z < 0)
		return nullptr;
	Chunk * chunk = nullptr;
	EnterCriticalSection(&worldUpdate);
	if (chunks.count(chunkHash(x, z)) > 0)
		chunk = chunks.at(chunkHash(x, z));
	if (load && loadChunk(x, z))
		chunk = chunks.at(chunkHash(x, z));
	LeaveCriticalSection(&worldUpdate);
	return chunk;
}

Chunk * World::getChunkFromBlock(int x, int y, int z, bool load)
{
	if (x < 0 || z < 0)
		return nullptr;
	x >>= Chunk::sizePow;
	z >>= Chunk::sizePow;
	return getChunk(x, z);
}

bool World::loadChunk(int x, int z)
{
	if (x < 0 || z < 0)
		return false;
	if (chunks.count(chunkHash(x, z)) > 0)
		return true;
	Chunk * chunk = new Chunk(x, z);
	chunk->world = this;
	EnterCriticalSection(&worldUpdate);
	chunks.insert(make_pair(chunkHash(x, z), chunk));
	LeaveCriticalSection(&worldUpdate);
	return true;
}

bool World::unloadChunk(int x, int z)
{
	if (x < 0 || z < 0)
		return false;
	if (chunks.count(chunkHash(x, z)) > 0)
	{
		Chunk * chunk = chunks.at(chunkHash(x, z));
		EnterCriticalSection(&worldUpdate);
		chunks.erase(chunkHash(x, z));
		LeaveCriticalSection(&worldUpdate);
		delete chunk;
		return true;
	}

	return false;
}

int World::getBlockId(int x, int y, int z)
{
	Chunk * chunk = getChunk(x >> Chunk::sizePow, z >> Chunk::sizePow);
	if (chunk != nullptr)
		return chunk->getBlockId(x & (Chunk::size - 1), y, z & (Chunk::size - 1));
	return 0;
}

void World::setBlockId(int id, int x, int y, int z)
{
	Chunk * chunk = getChunk(x >> Chunk::sizePow, z >> Chunk::sizePow);
	if (chunk != nullptr)
		chunk->setBlockId(id, x & (Chunk::size - 1), y, z & (Chunk::size - 1));
}

void World::updateChunk(int x, int z)
{
	Chunk * chunk = getChunk(x, z);
	if (chunk != nullptr)
	{
		chunk->onUpdate();
	}
}

void World::renderFromPoint(int x, int y, int z)
{
	x = (x - renderRadius / 2) < 0? 0 : (x - renderRadius / 2);
	z = (z - renderRadius / 2) < 0? 0 : (z - renderRadius / 2);
	for (int i = x; i < x + renderRadius; i++)
		for (int j = z; j < z + renderRadius; j++)
		{
			Chunk * chunk = getChunk(i, j);
			if (chunk != nullptr)
			{
				//if (!chunk->getMesh()->getIsUpdating())
					if (chunk->getMesh()->empty())
						TaskQueue::getInstance().addUpdateChunkTask(i, j);
					else
						chunk->getMesh()->render();

				/*if (chunk->getMesh()->empty())
					TaskQueue::getInstance().addUpdateChunkTask(i, j);
				else
					if (!chunk->getMesh()->getIsUpdating())
						chunk->getMesh()->render();*/
			}
			else
				TaskQueue::getInstance().addChunkLoadTask(i, j);
		}
}