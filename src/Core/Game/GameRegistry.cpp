#include "GameRegistry.h"

void GameRegistry::registerBlock(unsigned int id, Block * block)
{
	if (id == 0 || blocks[id] != nullptr)
		return;

	blocks[id] = block;
}

Block * GameRegistry::getBlock(unsigned int id)
{
	if (id == 0 || blocks[id] == nullptr)
		return 0;
	return blocks[id];
}

Block * GameRegistry::blocks[1024];