#include "Chunk.h"

Chunk::Chunk(int x, int z)
{
	chunkX = x;
	chunkZ = z;

	mesh = new Mesh();

	blocks = new int[size * size * height];

	PerlinNoise noise(0.5f, 1, 1, 6, 0, 1 / 128.f);
	
	for (int x = 0; x < size; x++)
		for (int z = 0; z < size; z++)
		{
			double hei = noise.GetHeight(x + (chunkX << sizePow), z + (chunkZ << sizePow)) * 64. + 64.;
			for (int y = 0; y < hei; y++)
				blocks[x + (z << sizePow) + (y << (2 * sizePow))] = 1;
			for (int y = hei; y < height; y++)
				blocks[x + (z << sizePow) + (y << (2 * sizePow))] = 0;
		}
}

Chunk::~Chunk()
{
	delete mesh;
}

int Chunk::getBlockId(int x, int y, int z)
{
	if (y < 0 || y > height || x < 0 || x > size || z < 0 || z > size)
		return 0;
	return blocks[x + (z << sizePow) + (y << (2 * sizePow))];
}

void Chunk::setBlockId(int id, int x, int y, int z)
{
	if (y < 0 || y > height || x < 0 || x > size || z < 0 || z > size)
		return ;
	blocks[x + (z << sizePow) + (y << (2 * sizePow))] = id;
	TaskQueue::getInstance().addUpdateChunkTask(chunkX, chunkZ);
}

void Chunk::onUpdate()
{
	mesh->clear();
	mesh->setIsUpdating(true);
	for (int x = 0; x < size; x++)
		for (int z = 0; z < size; z++)
			for (int y = 0; y < height; y++)
				if (getBlockId(x, y, z) != 0)
				{
					mesh->setOffset(x + (chunkX << sizePow), y, z + (chunkZ << sizePow));
					GameRegistry::getBlock(getBlockId(x, y, z))->render(world,
						x + (chunkX << sizePow), y, z + (chunkZ << sizePow));
				}
	mesh->setIsUpdating(false);
}

Mesh * Chunk::getMesh()
{
	return mesh;
}