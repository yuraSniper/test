#include "TaskQueue.h"

TaskQueue::TaskQueue()
{
	InitializeCriticalSection(&crit);
	head = tail = nullptr;
}

TaskQueue & TaskQueue::getInstance()
{
	static TaskQueue instance;
	return instance;
}

Task * TaskQueue::getTask()
{
	Task * task = nullptr;
	do
	{
		EnterCriticalSection(&crit);
		if (!empty())
			task = head;
		LeaveCriticalSection(&crit);
		Sleep(1);
	}
	while (task == nullptr);
	return task;
}

void TaskQueue::popTask()
{
	Task * task = head;
	head = head->next;
	delete task;
}

bool TaskQueue::empty()
{
	return head == nullptr;
}

void TaskQueue::push(Task * task)
{
	task->next = nullptr;
	EnterCriticalSection(&crit);
	if (!empty())
	{
		if (!contains(task))
		{
			for (Task * t = head, * pt = nullptr; t != nullptr; pt = t, t = t->next)
			{
				if (t->taskId > task->taskId)
				{
					if (pt == nullptr)
					{
						task->next = head;
						head = task;
					}
					else
					{
						task->next = pt->next;
						pt->next = task;
					}
					LeaveCriticalSection(&crit);
					return;
				}
			}
			tail->next = task;
			tail = task;
		}
	}
	else
		head = tail = task;
	LeaveCriticalSection(&crit);
}

bool TaskQueue::contains(Task * task)
{
	for (Task * t = head; t != nullptr; t = t->next)
		if (t->operator==(task))
			return true;
	return false;
}

void TaskQueue::addChunkLoadTask(int x, int z)
{
	Task * task = new Task;
	task->d1 = x;
	task->d2 = z;
	task->taskId = CHUNKLOAD;
	push(task);
}

void TaskQueue::addUpdateChunkTask(int x, int z)
{
	Task * task = new Task;
	task->d1 = x;
	task->d2 = z;
	task->taskId = CHUNKUPDATE;
	push(task);
}

void TaskQueue::addChunkUnloadTask(int x, int z)
{
	Task * task = new Task;
	task->d1 = x;
	task->d2 = z;
	task->taskId = CHUNKUNLOAD;
	push(task);
}