#pragma once

#include <Util\std.h>

enum TASK
{
	UNDEFINED = 0,
	CHUNKLOAD,
	CHUNKUPDATE,
	CHUNKUNLOAD
};

struct Task
{
	Task * next;
	TASK taskId;
	int d1, d2;
	bool operator==(Task * t)
	{
		if (taskId == t->taskId)
			if (d1 == t->d1 && d2 == t->d2)
				return true;
		return false;
	}
};

class TaskQueue
{
	Task * head, * tail;
	TaskQueue();
	TaskQueue(TaskQueue &);
	void operator=(TaskQueue &);
	CRITICAL_SECTION crit;
	void push(Task * task);
	bool contains(Task * task);
public:
	static TaskQueue & getInstance();
	Task * getTask();
	void popTask();
	bool empty();
	void addChunkLoadTask(int x, int z);
	void addUpdateChunkTask(int x, int z);
	void addChunkUnloadTask(int x, int z);
};