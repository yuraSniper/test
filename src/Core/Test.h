#pragma once

#include <Util\std.h>
#include <Util\Util.h>
#include <Util\Math\vec3.h>
#include <Util\Math\Perlin\PerlinNoise.h>

#include <Core\Window\Window.h>
#include <Core\Input\Input.h>
#include <Core\Game\Block.h>
#include <Core\TaskQueue.h>

class Test
{
	int frametime, ticktime;
	int oldframetime, oldticktime;

	//Temporary
	bool showFpsOrTime;
	//End of Temporary

	World * world;

	bool cursor, prevcursor;

	Window * window;
	Input * input;
	HANDLE pThread, taskThread;
	bool working;

	vec3 position, velocity;
	float pitch, yaw;

	void MainLoop();

	static void PreRender(void *, void *);
	static void TestRender(void *, void *);
	static void PostRender(void *, void *);

	static int ProcessThread(void *);
	static int TaskHandlerThread(void *);

public:
	Delegate Render, onInit;
	Test(Window * window, Input * input);
	void Init();
	void Shutdown();
};