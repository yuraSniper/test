#include "Test.h"

Test::Test(Window * wnd, Input * inp)
{
	window = wnd;
	input = inp;

	glewInit();

	Render.add(PreRender, this);
}

void Test::Init()
{
	onInit(nullptr);
	Render.add(TestRender, this);
	Render.add(PostRender, this);
	window->onDraw = &Render;
	glEnableClientState(GL_VERTEX_ARRAY);

	pThread = CreateThread(nullptr, 0, (LPTHREAD_START_ROUTINE)ProcessThread, this, CREATE_SUSPENDED, nullptr);
	taskThread = CreateThread(nullptr, 0, (LPTHREAD_START_ROUTINE)TaskHandlerThread, this, CREATE_SUSPENDED, nullptr);


	MainLoop();
	TerminateThread(pThread, 0);
}

void Test::MainLoop()
{
	working = true;
	MSG msg;
	position.z = -50;
	position.x = -50;
	position.y = 50;

	pitch = 0;
	yaw = 225;

	//Temporary
	GameRegistry::registerBlock(1, new Block(1));
	world = new World();
	//End of Temporary
	cursor = prevcursor = true;
	oldticktime = oldframetime = frametime = ticktime = 0;

	ResumeThread(pThread);
	ResumeThread(taskThread);

	while (working)
	{
		if (PeekMessage(&msg, nullptr, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
				Shutdown();
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
		Render(nullptr);
		Sleep(1);
		if (cursor != prevcursor)
		{
			window->showCursor(cursor);
			prevcursor = cursor;
		}
	}
}

void Test::PreRender(void * param, void *)
{
	static Test & This = *(Test *)param;
	static vec3 & pos = This.position;

	This.frametime = GetTickCount();
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glRotatef(-This.pitch, 1, 0, 0);
	glRotatef(-This.yaw, 0, 1, 0);
	glTranslatef(-pos.x, -pos.y, -pos.z);
}

void Test::PostRender(void * param, void *)
{
	static Test & This = *(Test *)param;
	static vec3 & pos = This.position;
	SwapBuffers(This.window->GetDC());
	This.frametime = GetTickCount() - This.frametime;
	//if (This.frametime != This.oldframetime || This.ticktime != This.oldticktime)
	//{
		This.oldframetime = This.frametime;
		This.oldticktime = This.ticktime;
		char str[129];
		if (This.showFpsOrTime)
			sprintf(str, "Test FPS : %f TPS : %f  X : %f Y : %f Z : %f", 1000. / This.oldframetime, 1000. / This.oldticktime, pos.x, pos.y, pos.z);
		else
			sprintf(str, "Test Frame time : %i Tick time : %i  X : %f Y : %f Z : %f", This.oldframetime, This.oldticktime, pos.x, pos.y, pos.z);
		This.window->setTitle(str);
	//}
}

void Test::TestRender(void * param, void *)
{
	static Test & This = *(Test *)param;
	static vec3 & pos = This.position;

	This.world->renderFromPoint(fastFloor(pos.x / Chunk::size), pos.y, fastFloor(pos.z / Chunk::size));

	glBegin(GL_LINES);
	glColor3f(1, 0, 0);
	glVertex3f(0, 0, 0);
	glVertex3f(1, 0, 0);

	glColor3f(0, 1, 0);
	glVertex3f(0, 0, 0);
	glVertex3f(0, 0, 1);

	glColor3f(0, 0, 1);
	glVertex3f(0, 0, 0);
	glVertex3f(0, 1, 0);
	glEnd();
}

void Test::Shutdown()
{
	working = false;
}

int Test::ProcessThread(void * param)
{
	static Test * This = (Test *)param;
	static Input & input = *This->input;
	HWND hWnd = This->window->GetHWND();
	vec3 & pos = This->position;
	vec3 & vel = This->velocity;
	float & pitch = This->pitch;
	float & yaw = This->yaw;
	float k = 0.02;
	int x, y, prevX, prevY;
	bool trapMouse = false;
	int time;

	POINT pt;
	GetCursorPos(&pt);
	ScreenToClient(hWnd, &pt);
	prevX = pt.x;
	prevY = pt.y;

	while (true)
	{
		time = GetTickCount();

		//Temporary
		if (input.GetKeyPressed(VK_F1))
			This->showFpsOrTime = !This->showFpsOrTime;

		x = input.GetMousePos(true);
		y = input.GetMousePos(false);

		pos = pos + vel;
		vel = vel * 0.9;

		if (input.GetKey('W'))
		{
			vel.z -= cos(deg2rad(yaw)) * k;
			vel.x -= sin(deg2rad(yaw)) * k;
		}

		if (input.GetKey('S'))
		{
			vel.z += cos(deg2rad(yaw)) * k;
			vel.x += sin(deg2rad(yaw)) * k;
		}

		if (input.GetKey('A'))
		{
			vel.z -= cos(deg2rad(yaw + 90)) * k;
			vel.x -= sin(deg2rad(yaw + 90)) * k;
		}

		if (input.GetKey('D'))
		{
			vel.z += cos(deg2rad(yaw + 90)) * k;
			vel.x += sin(deg2rad(yaw + 90)) * k;
		}

		if (input.GetKey(' '))
		{
			vel.y += k;
		}

		if (input.GetKey(VK_LSHIFT))
		{
			vel.y -= k;
		}

		if (input.GetKeyPressed(VK_ESCAPE))
		{
			trapMouse = !trapMouse;
			prevX = x;
			prevY = y;
		}

		if (trapMouse)
		{
			RECT rt, rt2;
			POINT pt;
			GetClientRect(hWnd, &rt);
			pt.x = rt.right - 1;
			pt.y = rt.bottom - 1;
			ClientToScreen(hWnd, &pt);
			rt2.right = pt.x;
			rt2.bottom = pt.y;
			pt.x = pt.y = 0;
			ClientToScreen(hWnd, &pt);
			rt2.left = pt.x;
			rt2.top = pt.y;

			ClipCursor(&rt2);
			if (abs(x - rt.left) <= 10 || abs(x - rt.right) <= 10 || abs(y - rt.top) <= 10 || abs(y - rt.bottom) <= 10)
			{
				POINT p = {rt.left + (rt.right - rt.left) / 2, rt.top + (rt.bottom - rt.top) / 2};
				x = prevX = p.x;
				y = prevY = p.y;
				ClientToScreen(hWnd, &p);
				SetCursorPos(p.x, p.y);
			}
			This->cursor = false;
			if (x != prevX || y != prevY)
			{
				yaw -= (x - prevX) * 0.5;
				pitch -= (y - prevY) * 0.5;

				if (pitch > 89)
					pitch = 89;
				if (pitch < -89)
					pitch = -89;
				if (yaw > 360)
					yaw -= 360;
				if (yaw < 0)
					yaw = 360 - yaw;

				prevX = x;
				prevY = y;
			}
		}
		else
		{
			ClipCursor(nullptr);
			This->cursor = true;
		}

		This->ticktime = GetTickCount() - time;
		Sleep(1);
	}
}

int Test::TaskHandlerThread(void * param)
{
	static Test & This = *(Test*)param;

	while (true)
	{
		Task * task = TaskQueue::getInstance().getTask();
		switch (task->taskId)
		{
			case CHUNKLOAD:
				This.world->loadChunk(task->d1, task->d2);
				break;
			case CHUNKUNLOAD:
				This.world->unloadChunk(task->d1, task->d2);
				break;
			case CHUNKUPDATE:
				This.world->updateChunk(task->d1, task->d2);
				break;
		}
		TaskQueue::getInstance().popTask();
		Sleep(1);
	}
}