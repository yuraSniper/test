#include <Util\std.h>
#include <Core\Window\Window.h>
#include <Core\Input\Input.h>
#include <Core\Test.h>

#pragma comment(lib, "OpenGl32.lib")
#pragma comment(lib, "Glu32.lib")

int __stdcall WinMain(HINSTANCE hInst, HINSTANCE, LPSTR, int)
{
	Window * window = new Window(hInst, L"Test", 800, 600);
	Input * input = new Input(window);
	Test * test = new Test(window, input);
	test->Init();
	delete test;
	return 0;
}