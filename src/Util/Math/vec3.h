#pragma once

#include <Util/std.h>

class vec3
{
public:
	float x, y, z;
	vec3(float x, float y, float z);
	vec3();
	vec3 operator+(const vec3 & v);
	vec3 operator-(const vec3 & v);
	vec3 operator*(float k);
	vec3 & operator=(const vec3 & v);
	bool operator==(const vec3 & v);
	float dot(const vec3 & v);
	vec3 cross(const vec3 & v);
	float len();
	void normalize(float k = 1);
};