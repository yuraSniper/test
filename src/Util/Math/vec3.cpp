#include "vec3.h"

vec3::vec3()
{
	x = y = z = 0;
}

vec3::vec3(float x, float y, float z)
{
	this->x = x;
	this->y = y;
	this->z = z;
}

vec3 vec3::operator+(const vec3 & v)
{
	return vec3(x + v.x, y + v.y, z + v.z);
}

vec3 vec3::operator-(const vec3 & v)
{
	return vec3(x - v.x, y - v.y, z - v.z);
}

vec3 vec3::operator*(float k)
{
	return vec3(x * k, y * k, z * k);
}

vec3 & vec3::operator=(const vec3 & v)
{
	x = v.x;
	y = v.y;
	z = v.z;
	return *this;
}

float vec3::dot(const vec3 & v)
{
	return x * v.x + y * v.y + z * v.z;
}

vec3 vec3::cross(const vec3 & v)
{
	vec3 tmp(y * v.z - z * v.y, -(x * v.z - z * v.x), x * v.y - y * v.x);
	return tmp;
}

float vec3::len()
{
	return sqrt(x * x + y * y + z * z);
}

void vec3::normalize(float k)
{
	float l = len();
	if (l != 0)
		*this = *this * (1 / len()) * k;
}

bool vec3::operator==(const vec3 & v)
{
	return x == v.x && y == v.y && z == v.z;
}
