#include "Random.h"

unsigned int Random::next(int bits)
{
	seed = seed * 0xDEECE66D + 0xB;
	return seed >> (32 - bits);
}

Random::Random()
{
	setSeed(time(nullptr));
}

Random::Random(unsigned int seed)
{
	setSeed(seed);
}

void Random::setSeed(unsigned int seed)
{
	this->seed = seed ^ 0xDEECE66D;
}

unsigned int Random::nextInt(unsigned int max)
{
	return next(31) % max;
}

double Random::nextDouble()
{
	return (double)next(31) / (1 << 31);
}