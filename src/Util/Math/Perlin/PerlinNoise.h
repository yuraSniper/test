#pragma once

class PerlinNoise
{
	double persistence, frequency, amplitude, mult, randomseed;
	int octaves;

	double Total(double i, double j);
	double GetValue(double x, double y);
	double Interpolate(double x, double y, double a);
	double Noise(int x, int y);

public:
	PerlinNoise(double _persistence, double _frequency, double _amplitude,
		int _octaves, double _randomseed, double _mult);

	double GetHeight(double x, double y);
	void setSeed(double seed);
};