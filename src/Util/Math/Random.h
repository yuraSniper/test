#pragma once

#include <Util\std.h>

class Random
{
	unsigned int seed;
protected:
	unsigned int next(int bits);
public:
	Random();
	Random(unsigned int seed);
	void setSeed(unsigned int seed);
	unsigned int nextInt(unsigned int max = MAXUINT32);
	double nextDouble();
};