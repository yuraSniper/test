#pragma once

#include "std.h"

inline float deg2rad(float x)
{
	return M_PI / 180. * x;
}

inline double fastFloor(double x)
{
	return x < 0? (int)x - 1 : (int)x;
}