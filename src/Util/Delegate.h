#pragma once

#include <vector>

using namespace std;

class Delegate
{
	struct Func
	{
		void (*func)(void*, void*);
		void * param;
	};
	vector< Func > list;

public:

	void add(void (func)(void*, void*), void * param)
	{
		Func tmp;
		tmp.func = func;
		tmp.param = param;
		list.push_back(tmp);
	}

	void del(void (func)(void*, void*), void * param)
	{
		if (!list.empty())
			for (int i = 0; i < list.size(); i++)
				if (list[i].param == param && list[i].func == func)
					list.erase(list.begin() + i);
	}
	void operator()(void * arg)
	{
		if (!list.empty())
			for (int i = 0; i < list.size(); i++)
				(*list[i].func)(list[i].param, arg);
	}
};